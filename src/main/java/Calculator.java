import java.util.Scanner;
class Calci
{
	public double add(double firstNumber,double secondNumber)
	{
		return firstNumber+secondNumber;
	}
	public double subtract(double firstNumber,double secondNumber)
	{
		return firstNumber-secondNumber;
	}
	public double multiply(double firstNumber,double secondNumber)
	{
		return firstNumber*secondNumber;
	}
	public double divide(double firstNumber,double secondNumber)
	{
		if(secondNumber==0)
		{
			System.out.println("Division is not possible");
			return 0;
		}
		else
		{
			return firstNumber/secondNumber;
		}
	}
}
public class Calculator
{
	
    public static void main( String[] args )
    {
    	Scanner scan=new Scanner(System.in);
    	System.out.println("Enter two numbers:");
    	Calci calculator=new Calci();
    	double first,second;
    	first=scan.nextDouble();
    	second=scan.nextDouble();
    	int option;
    	double output=0;
    	System.out.println("\nSelect any one of these: \n1.Addition\n2.Subtraction"+ "\n3.Multiplication\n4.Division");
    	System.out.println("\nplease enter your choice:");
    	option=scan.nextInt();
    	switch(option)
    	{
    		case 1: output= calculator.add(first,second);
    				System.out.println("The answer is :"+output);
    				break;
    		case 2: output=calculator.subtract(first, second);
    				System.out.println("The answer is :"+output);
    				break;
    		case 3: output=calculator.multiply(first, second);
    				System.out.println("The answer is :"+output);
    				break;
    		case 4: output=calculator.divide(first, second);
    				break;
    		default:System.out.println("Please enter correct choice for the next time");
    				
    	}
    	
    }
}